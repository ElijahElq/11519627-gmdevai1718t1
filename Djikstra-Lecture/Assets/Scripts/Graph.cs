﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Graph : MonoBehaviour {

    [SerializeField]
    protected List<Node> nodes = new List<Node>();

    public virtual List<Node> getNodes
    {
        get { return nodes; }
    }

    public virtual Path GetDijkstraPath(Node start, Node end)
    {
        //We dont want null arguments
        if (start == null || end == null)
        {
            throw new ArgumentNullException();
        }

        Path path = new Path();

        // If the start and end nodes are the same, we can return the start node
        if (start == end)
        {
            path.getNodes.Add(start);
            return path;
        }

        List<Node> unvisited = new List<Node>();

        // Previous nodes in optimal path from source
        Dictionary<Node, Node> previous = new Dictionary<Node, Node>();

        //The calculated distances
        Dictionary<Node, float> distances = new Dictionary<Node, float>();

        //Set all nodes to infinity except Start Node
        for (int i = 0; i < nodes.Count; i++)
        {
            Node node = nodes[i];
            unvisited.Add(node);

            // setting the node distance to infinity
            distances.Add(node, float.MaxValue);
        }

        //Set the starting node distance = 0
        distances[start] = 0f;

        while (unvisited.Count != 0)
        {
            // Sort the unvisited list by distance, smallest distance at start and largest at the end
            unvisited = unvisited.OrderBy(node => distances[node]).ToList();

            // Get the node with the smallest distance
            Node current = unvisited[0];

            // Remove the current node
            unvisited.Remove(current);

            // When the current is equal to the end node, then we can, then we can break and return the path
            if (current == end)
            {
                // Construct shortest path
                while (previous.ContainsKey(current))
                {
                    // Insert the node onto the final result
                    path.getNodes.Insert(0, current);

                    // Traverse from start to end
                    current = previous[current];
                }
                // Insert the source onto the final result
                path.getNodes.Insert(0, current);
                break;
            }

            // Looping through the Node connections(neightbors) and where the connection is available at unvisited list
            for (int i = 0; i < current.getConnections.Count; i++)
            {
                Node neightbor = current.getConnections[i];

                //Getting the distance between the current node and the connection
                float length = Vector3.Distance(current.transform.position, neightbor.transform.position);

                // the distance from start node to this connection of the current node
                float alt = distances[current] + length;

                // If a shorter path to the connection has been found
                if (alt < distances[neightbor])
                {
                    distances[neightbor] = alt;
                    previous[neightbor] = current;
                }
            }
        }
        path.Bake();
        return path;
    }
}
